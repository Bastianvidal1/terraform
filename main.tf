#########################
#Azure resoruce provider
#########################
provider "azurerm" {
  version = "=2.36.0"
  features {}
}

########################3#####
#Private key RSA  for AKS
###############################
resource "tls_private_key" "iac" {
  algorithm = "RSA"

}

#######################
# Data Resource group #
#######################
data "azurerm_resource_group" "iac" {
  name = var.resource_group_name
}


################################
# Azure Storage Account Module #
################################
module "azure_storage_account" {
  source = "./modules/asa"
  storage_account_name = var.storage_account_name
  resource_group_name = data.azurerm_resource_group.iac.name
  account_tier = var.account_tier
  replication_type = var.replication_type
  environment = var.environment
  storage_container_name = var.storage_container_name
  container_access_type = var.container_access_type
}

###################################
# Azure Container Registry Module #
###################################
module "azure_container_registry" {
    source = "./modules/acr"
    resource_group_name = data.azurerm_resource_group.iac.name
    container_registry_name = var.container_registry_name
    sku = var.acr_sku
}

###################################
# Azure Kubernetes Service Module #
###################################
module "azure_kubernetes_cluster"{
  source = "./modules/aks"
  name = var.cluster_name
  resource_group_name = data.azurerm_resource_group.iac.name
  dns_prefix = var.dns_prefix
  agent_vm_count = var.agent_count
  agent_vm_size  = "Standard_D2_V2"
  admin_user = var.admin_username
  ssh_public_key = "${trimspace(tls_private_key.iac.public_key_openssh)} ${var.admin_username}@azure.com"
  agent_count = var.agent_count
  output_directory = var.output_directory
  kubeconfig_filename = var.kubeconfig_filename
}

################################
# Azure Role Assignment Module #
################################
module "azure_role_assignment_k8_acr" {
  source = "./modules/ara"
  principal_id = module.azure_kubernetes_cluster.kubelet_identity[0].object_id
  role_definition_name = var.role_definition_name_for_k8
  scope = module.azure_container_registry.id
  skip_service_principal_aad_check = var.skip_service_principal_aad_check
}

################################
# Push Docker HUB Image To ACR #
################################
resource "null_resource" "image_push_to_acr" {
  
  provisioner "local-exec" {
    command = <<EOF
        az acr import  -n ${module.azure_container_registry.name} --source docker.io/library/nginx:latest --image nginx:v1
    EOF
  }
}

######################
# Deploy YAML To AKS #
######################
resource "null_resource" "deploy" {
  provisioner "local-exec" {
    ## Example Deployment File due the ACR can´t be associated with the cluster
    /*interpreter = ["PowerShell","-Command"]*/
    command = <<EOF
      kubectl apply -f https://k8s.io/examples/controllers/nginx-deployment.yaml --kubeconfig ${module.azure_kubernetes_cluster.cluster_credentials[0].filename}
    EOF

  }

  provisioner "local-exec" {
    command = <<EOF
      kubectl apply -f azure-vote-all-in-one-redis.yaml --kubeconfig ${module.azure_kubernetes_cluster.cluster_credentials[0].filename}
    EOF
  }

  depends_on = [
    module.azure_kubernetes_cluster
  ]
}