variable "location" {
  default = "East US"
}

variable "resource_group_name" {
  type = string
  default = "CLOUD_BOOTCAMP_2022_BVIDAL"

}
#//akz-<nombre-grupo>-<solucion-nombre>-<evn>-<version>
#//aks-iac-demo-dev-01 //  32 cares max, no palabras

variable "cluster_name" { 
  default = "aksdemo1"  

}

variable "agent_count" {
  default = 3
}

variable "dns_prefix" {
  default = "aksdemo"

}

variable "admin_username" {
  default = "aksdemo"

}

variable "output_directory" {
  default = "./output"

}

variable "kubeconfig_filename" {
  default = "aks-config"

}

variable "ssh_public_key" {
  type = string
  default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCuqHznsEXDYh6AItTyKLNpWg+qhouhM/AIrdL2e608aufA3R/MhuSe8XQbkoujP7EPmaYxmTzAxQJirWAcIh1enF0QTt7I4CS9qcmcdta/delybOgj+MW5FxPPWt/gGzZneJ6gOEA4mvnvc0IbAz3/Q8HQMI41BV8LJl2/mZDskoa2vphUa72bJy+8/hICpDxZxHl+MM0VMIxBN6SuCg3UmRGDiPbYG2oW36WQUkXqHsXznREyyoncgxYQlIRha9EZgToMr+EXsnWEjKLYVJOGy89HIUJ8KuMVW0dor2Fe/LGrWpj3d8GcajUlbkWv9yjFQ7wNi6t6UAo4N/L11RqhdEiEQ3ODdWavlKNkZb7lYkncf3e4ks3hKMc/USKOPs1aZl8h2I1oJU/Nm4UqtAkuOZme+Ki8E90TGur1YU++jE0I/1VHqnimcVeHxAfsQX542LHH1aWsx5vMyskHtXD736nrEJrIg2FAg3GdkObyuk9KQpiryBnI2MGoRKhjmYc= usersad\\bvidalto@SAN-FYQR493"
}

variable "storage_account_name" {
  default = "bvidaltoproject"
}

variable "account_tier" {
  default = "Standard"
}

variable "replication_type" {
  default = "GRS"
}

variable "environment" {
  default = "Development"
}

variable "storage_container_name" {
  default = "proyect-storage-container"
}

variable "container_access_type" {
  default = "private"
}

variable "container_registry_name" {
  default = "ACRK8bvidal"
}

variable "acr_sku" {
  default = "Basic"  
}

variable "role_definition_name_for_k8" {
  default = "AcrPull"
}

variable "skip_service_principal_aad_check" {
  default = true
}