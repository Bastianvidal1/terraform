data "azurerm_resource_group" "iac" {
    name = var.resource_group_name
}

resource "azurerm_container_registry" "acr" {
  name = var.container_registry_name
  resource_group_name = data.azurerm_resource_group.iac.name
  location = data.azurerm_resource_group.iac.location
  sku = var.sku
}