variable "account_tier" {
  type = string
  default = "Standard"
}

variable "replication_type" {
  type = string
  default = "GRS"
}

variable "environment" {
  type = string
}

variable "storage_container_name" {
  type = string
}

variable "container_access_type" {
  type = string
}

variable "storage_account_name" {
  type = string
}

variable "resource_group_name" {
  type = string
}