data "azurerm_resource_group" "iac" {
    name = var.resource_group_name
}

resource "azurerm_storage_account" "asa" {
  name = var.storage_account_name
  resource_group_name = data.azurerm_resource_group.iac.name
  location = data.azurerm_resource_group.iac.location
  account_tier = var.account_tier
  account_replication_type = var.replication_type
  
  tags = {
    environment = var.environment
  }
}

resource "azurerm_storage_container" "asa" {
  name = var.storage_container_name
  storage_account_name = azurerm_storage_account.asa.name
  container_access_type = var.container_access_type
}

