
data "azurerm_resource_group" "iac" {
    name = var.resource_group_name
}

###############
# AKS Cluster #
###############
resource "azurerm_kubernetes_cluster" "iac" {
    name = var.name
    resource_group_name = data.azurerm_resource_group.iac.name
    location = data.azurerm_resource_group.iac.location
    dns_prefix = var.dns_prefix

    linux_profile {
      admin_username = var.admin_user
      ssh_key{
        key_data = var.ssh_public_key
      }
    }

    default_node_pool {
      name = "default"
      node_count = var.agent_vm_count
      vm_size = var.agent_vm_size
    }
    identity {
      type = "SystemAssigned"
    }

    tags = {
      Enviroment = "Develop"
    }
    

}

#####################
# Local Kube Config #
#####################
resource "local_file" "cluster_credentials" {
    count = var.agent_count 
    sensitive_content = azurerm_kubernetes_cluster.iac.kube_config_raw
    filename = "${var.output_directory}/${var.kubeconfig_filename}"
    depends_on = [
      azurerm_kubernetes_cluster.iac
    ]
}