output "id" {
  value = azurerm_kubernetes_cluster.iac.id
}

output "kube_config" {
  value = azurerm_kubernetes_cluster.iac.kube_config_raw
  sensitive = true
}

output "kubelet_identity" {
  value = azurerm_kubernetes_cluster.iac.kubelet_identity
}

output "kube_admin_config" {
  sensitive = true
  value = azurerm_kubernetes_cluster.iac.kube_config
}

output "cluster_credentials" {
  value = local_file.cluster_credentials
}